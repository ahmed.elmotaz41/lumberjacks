namespace Lumberjacks.Problem
{
    public class LogPrice
    {
        public int length { get; }

        public int price { get; }

        public LogPrice(int length, int price)
        {
            this.length = length;
            this.price = price;
        }
    }
}