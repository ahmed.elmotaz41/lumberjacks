﻿namespace Lumberjacks
{
    static class Program
    {
        public static void Main()
        {
            int testValue;
            do
            {
                Console.Write("\nEnter your choice: [1] Trial Cases [2] Sample Test Cases [3] Complete Test Cases... [any key for exit] ");
                testValue = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
            } while (new[] {1,2,3}.Contains(testValue));
        }
    }
}