# Lumberjacks

## Description:
Once upon a time, in a kingdom nestled in a lush forest, there were skilled woodcutters who would venture into the woods to gather logs to sell in the marketplace. The woodcutters were experts at their craft, but they faced a challenge - they had to cut the logs into pieces of different sizes, but different sizes of wood had different values in the market.

To solve this problem, the kingdom turned to a wise old wizard, who suggested that they use an algorithm to optimize their log cutting strategy. The wizard explained that they could input the market price of each size they needed to cut, and the algorithm would analyze the data and provide a recommendation on how to cut the logs to maximize profits.

You will have a function``` long CutLogs(List<LogPrice> logPrice, List<int> logPiece): ```
- logPrice List<LogPrice>
- logPieces List<int>
- Returns the maximum integer profit.

There will be multiple test cases some of them will be with you and the other we will be hidden, you are required to find an algorithms to find the most optimal way to maximize the profite from the logPiece to increase the profite according to logPrice.

## Input:
The logPrices array and logPiece size within ```(0 < size < 10^5)```.

## Output:
It will be a single integer representing the maximum profit ```(0 <= profit <= 10^18)```.