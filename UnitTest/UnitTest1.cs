using Lumberjacks.Problem;

namespace Lumberjacks.UnitTest
{
    using Xunit;

    public class UnitTest1
    {
        [Theory]
        [InlineData("sample1")]
        public void TestSample(string samplePath)
        {
            SampleInfo sampleInfo = getSampleInfo(samplePath);
            long answer = Problem.Lumberjacks.CutLogs(sampleInfo.logPrice!, sampleInfo.logPiece!);
            Assert.Equal(answer, sampleInfo.expected);
        }

        private SampleInfo getSampleInfo(string samplePath)
        {
            if (samplePath == "sample1")
            {
                return new SampleInfo
                {
                    logPrice = new List<LogPrice> { new(7, 9), new(2, 2), new(4, 5) },
                    logPiece = new List<int> { 12, 4, 7, 5, 3 },
                    expected = 35,
                };
            }
            throw new NotImplementedException();
        }
    }
}