namespace Lumberjacks.UnitTest
{
    using Lumberjacks.Problem;

    public class SampleInfo
    {
        public List<LogPrice>? logPrice { get; set; }

        public List<int>? logPiece { get; set; }

        public int expected { get; set; }
    }
}